package by.itstep.feedback.service;

import by.itstep.feedback.dto.feedbackComment.FeedbackCommentCreateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentFullDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentUpdateDto;
import by.itstep.feedback.entity.FeedbackCommentEntity;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.exceptions.FeedbackCommentNotFoundException;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import by.itstep.feedback.repository.FeedbackCommentRepository;
import by.itstep.feedback.repository.FeedbackRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.List;
import static by.itstep.feedback.repository.EntityGenerationUtils.*;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackCommentCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackCommentUpdateDto;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FeedbackCommentServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        //given
        FeedbackCommentEntity entity = generateFeedbackCommentWithFeedback();

        FeedbackCommentEntity saved = feedbackCommentRepository.save(entity);

        //when
        FeedbackCommentFullDto foundDto = feedbackCommentService.findById(saved.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getMessage(), foundDto.getMessage());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<FeedbackCommentEntity> toSave = generateFeedbackCommentsWithFeedback();

        List<FeedbackCommentEntity> saved = feedbackCommentRepository.saveAll(toSave);

        // when
        Page<FeedbackCommentFullDto> foundFeedbackComments = feedbackCommentService.findAll(0, 3);
        List<FeedbackCommentFullDto> feedbackComments = foundFeedbackComments.getContent();

        // then
        Assertions.assertNotNull(foundFeedbackComments);
        Assertions.assertEquals(saved.size(), feedbackComments.size());
    }

    @Test
    void create_happyPath() {
        // given
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDtoWithFeedback();

        // when
        FeedbackCommentFullDto saved = feedbackCommentService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(saved.getCreateTime());
        Assertions.assertEquals(createDto.getImageUrl(), saved.getImageUrl());
        Assertions.assertEquals(createDto.getMessage(), saved.getMessage());
    }

    @Test
    void update_happyPath() {
        // given
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDtoWithFeedback();

        FeedbackCommentFullDto saved = feedbackCommentService.create(createDto);

        FeedbackCommentUpdateDto updateDto = generateFeedbackCommentUpdateDto();
        updateDto.setId(saved.getId());

        // when
        FeedbackCommentFullDto updated = feedbackCommentService.update(updateDto);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertNotEquals(saved.getMessage(), updated.getMessage());
        Assertions.assertNotEquals(saved.getImageUrl(), updated.getImageUrl());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
    }

    @Test
    void deleteById_happyPath() {
        // given
        FeedbackCommentFullDto feedbackCommentSaved = feedbackCommentService.create(generateFeedbackCommentCreateDtoWithFeedback());

        // when
        feedbackCommentService.deleteById(feedbackCommentSaved.getId());

        Exception exception = assertThrows(FeedbackCommentNotFoundException.class,
                () -> feedbackCommentService.findById(feedbackCommentSaved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(feedbackCommentSaved.getId())));
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(FeedbackCommentNotFoundException.class,
                () -> feedbackCommentService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound() {
        // given
        FeedbackCommentUpdateDto updateDto = generateFeedbackCommentUpdateDto();

        Integer notExistingId = 100;
        updateDto.setId(notExistingId);

        // when
        Exception exception = Assertions.assertThrows(FeedbackCommentNotFoundException.class,
                () -> feedbackCommentService.update(updateDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(FeedbackCommentNotFoundException.class,
                () -> feedbackCommentService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    public FeedbackCommentEntity generateFeedbackCommentWithFeedback() {
        FeedbackCommentEntity feedbackComment = generateFeedbackComment();

        feedbackComment.setFeedback(feedbackRepository.save(generateFeedback()));

        return feedbackComment;
    }

    public List<FeedbackCommentEntity> generateFeedbackCommentsWithFeedback() {
        List<FeedbackCommentEntity> toSave = generateFeedbackComments();

        for (FeedbackCommentEntity entity : toSave) {
            entity.setFeedback(feedbackRepository.save(generateFeedback()));
        }

        return toSave;
    }

    public FeedbackCommentCreateDto generateFeedbackCommentCreateDtoWithFeedback() {
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDto();

        FeedbackEntity feedback = feedbackRepository.save(generateFeedback());

        createDto.setFeedbackId(feedback.getId());

        return createDto;
    }
}
