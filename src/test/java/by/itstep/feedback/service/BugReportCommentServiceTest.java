package by.itstep.feedback.service;

import by.itstep.feedback.dto.bugReportComment.BugReportCommentCreateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentFullDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentUpdateDto;
import by.itstep.feedback.entity.BugReportCommentEntity;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.exceptions.BugReportCommentNotFoundException;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import by.itstep.feedback.repository.BugReportCommentRepository;
import by.itstep.feedback.repository.BugReportRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.List;

import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportCommentCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportCommentUpdateDto;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static by.itstep.feedback.repository.EntityGenerationUtils.*;

public class BugReportCommentServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        BugReportCommentEntity entity = generateBugReportCommentWithBugReport();

        BugReportCommentEntity saved = bugReportCommentRepository.save(entity);

        // when
        BugReportCommentFullDto foundDto = bugReportCommentService.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getMessage(), foundDto.getMessage());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<BugReportCommentEntity> toSave = generateBugReportCommentsWithBugReport();

        List<BugReportCommentEntity> saved = bugReportCommentRepository.saveAll(toSave);

        // when
        Page<BugReportCommentFullDto> foundBugReportComments = bugReportCommentService.findAll(0, 3);
        List<BugReportCommentFullDto> bugReportComments = foundBugReportComments.getContent();

        // then
        Assertions.assertNotNull(foundBugReportComments);
        Assertions.assertEquals(bugReportComments.size(), saved.size());
        
    }

    @Test
    void create_happyPath() {
        // given
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDtoWithBugReport();

        // when
        BugReportCommentFullDto saved = bugReportCommentService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(saved.getCreateTime());
        Assertions.assertEquals(createDto.getImageUrl(), saved.getImageUrl());
        Assertions.assertEquals(createDto.getMessage(), saved.getMessage());
    }

    @Test
    void update_happyPath() {
        // given
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDtoWithBugReport();

        BugReportCommentFullDto saved = bugReportCommentService.create(createDto);

        BugReportCommentUpdateDto updateDto = generateBugReportCommentUpdateDto();
        updateDto.setId(saved.getId());

        // when
        BugReportCommentFullDto updated = bugReportCommentService.update(updateDto);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertNotEquals(saved.getMessage(), updated.getMessage());
        Assertions.assertNotEquals(saved.getImageUrl(), updated.getImageUrl());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
    }

    @Test
    void deleteById_happyPath() {
        // given
        BugReportCommentFullDto bugReportCommentSaved = bugReportCommentService.create(generateBugReportCommentCreateDtoWithBugReport());

        // when
        bugReportCommentService.deleteById(bugReportCommentSaved.getId());

        Exception exception = assertThrows(BugReportCommentNotFoundException.class,
                () -> bugReportCommentService.findById(bugReportCommentSaved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(bugReportCommentSaved.getId())));
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(BugReportCommentNotFoundException.class,
                () -> bugReportCommentService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound() {
        // given
        BugReportCommentUpdateDto updateDto = generateBugReportCommentUpdateDto();

        Integer notExistingId = 100;
        updateDto.setId(notExistingId);

        // when
        Exception exception = Assertions.assertThrows(BugReportCommentNotFoundException.class,
                () -> bugReportCommentService.update(updateDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(updateDto.getId())));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(BugReportCommentNotFoundException.class,
                () -> bugReportCommentService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }


    public BugReportCommentEntity generateBugReportCommentWithBugReport() {
        BugReportCommentEntity bugReportComment = generateBugReportComment();

        bugReportComment.setBugReport(bugReportRepository.save(generateBugReport()));

        return bugReportComment;
    }


    public List<BugReportCommentEntity> generateBugReportCommentsWithBugReport() {
        List<BugReportCommentEntity> toSave = generateBugReportComments();

        for (BugReportCommentEntity entity : toSave) {
            entity.setBugReport(bugReportRepository.save(generateBugReport()));
        }

        return toSave;
    }

    public BugReportCommentCreateDto generateBugReportCommentCreateDtoWithBugReport() {
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDto();

        BugReportEntity bugReport = bugReportRepository.save(generateBugReport());

        createDto.setBugReportId(bugReport.getId());

        return createDto;
    }


}
