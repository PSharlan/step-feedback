package by.itstep.feedback.service;

import by.itstep.feedback.dto.feedback.FeedbackCreateDto;
import by.itstep.feedback.dto.feedback.FeedbackFullDto;
import by.itstep.feedback.dto.feedback.FeedbackPreviewDto;
import by.itstep.feedback.dto.feedback.FeedbackUpdateDto;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.entity.enums.FeedbackEntityStatus;
import by.itstep.feedback.exceptions.FeedbackNotFoundException;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import by.itstep.feedback.repository.FeedbackRepository;
import liquibase.pro.packaged.F;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.List;
import static by.itstep.feedback.repository.EntityGenerationUtils.*;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackUpdateDto;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FeedbackServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        FeedbackEntity entity = generateFeedback();

        entity.setComments(generateFeedbackComments());

        FeedbackEntity saved = feedbackRepository.save(entity);

        // when
        FeedbackFullDto foundDto = feedbackService.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getStatus(), foundDto.getStatus());
        Assertions.assertEquals(saved.getMessage(), foundDto.getMessage());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
        Assertions.assertEquals(saved.getComments().size(), saved.getComments().size());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<FeedbackEntity> toSave = generateFeedbacks();

        List<FeedbackEntity> saved = feedbackRepository.saveAll(toSave);

        // when
        Page<FeedbackPreviewDto> foundFeedbacks = feedbackService.findAll(0, 3);
        List<FeedbackPreviewDto> feedbacks = foundFeedbacks.getContent();

        // then
        Assertions.assertNotNull(foundFeedbacks);
        Assertions.assertEquals(saved.size(), feedbacks.size());

        Assertions.assertEquals(saved.get(0).getUserId(), feedbacks.get(0).getUserId());
        Assertions.assertEquals(saved.get(0).getMessage(), feedbacks.get(0).getMessage());

    }

    @Test
    void create_happyPath() {
        // given
        FeedbackCreateDto createDto = generateFeedbackCreateDto();

        // when
        FeedbackFullDto saved = feedbackService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(saved.getCreateTime());
        Assertions.assertNotNull(saved.getStatus());
        Assertions.assertEquals(createDto.getMessage(), saved.getMessage());
        Assertions.assertEquals(createDto.getUserId(), saved.getUserId());
    }

    @Test
    void update_happyPath() {
        // given
        FeedbackCreateDto createDto = generateFeedbackCreateDto();

        FeedbackFullDto saved = feedbackService.create(createDto);

        FeedbackUpdateDto updateDto = generateFeedbackUpdateDto();
        updateDto.setId(saved.getId());

        // when
        FeedbackFullDto updated = feedbackService.update(updateDto);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertNotEquals(saved.getMessage(), updated.getMessage());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
    }

    @Test
    void deleteById_happyPath() {
        // given
        FeedbackFullDto feedbackSaved = feedbackService.create(generateFeedbackCreateDto());

        // when
        feedbackService.deleteById(feedbackSaved.getId());

        Exception exception = assertThrows(FeedbackNotFoundException.class,
                () -> feedbackService.findById(feedbackSaved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(feedbackSaved.getId())));
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(FeedbackNotFoundException.class,
                () -> feedbackService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound() {
        // given
        FeedbackUpdateDto updateDto = generateFeedbackUpdateDto();

        Integer notExistingId = 100;
        updateDto.setId(notExistingId);

        // when
        Exception exception = Assertions.assertThrows(FeedbackNotFoundException.class,
                () -> feedbackService.update(updateDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(FeedbackNotFoundException.class,
                () -> feedbackService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }


}

