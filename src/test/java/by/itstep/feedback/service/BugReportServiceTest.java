package by.itstep.feedback.service;

import by.itstep.feedback.dto.bugReport.BugReportCreateDto;
import by.itstep.feedback.dto.bugReport.BugReportFullDto;
import by.itstep.feedback.dto.bugReport.BugReportPreviewDto;
import by.itstep.feedback.dto.bugReport.BugReportUpdateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentFullDto;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.entity.enums.BugReportEntityStatus;
import by.itstep.feedback.exceptions.BugReportNotFoundException;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import by.itstep.feedback.repository.BugReportRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportUpdateDto;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BugReportServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        BugReportEntity entity = generateBugReport();

        entity.setComments(generateBugReportComments());

        BugReportEntity saved = bugReportRepository.save(entity);

        // when
        BugReportFullDto foundDto = bugReportService.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getStatus(), foundDto.getStatus());
        Assertions.assertEquals(saved.getMessage(), foundDto.getMessage());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
        Assertions.assertEquals(saved.getComments().size(), saved.getComments().size());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<BugReportEntity> toSave = generateBugReports();

        List<BugReportEntity> saved = bugReportRepository.saveAll(toSave);

        // when
        Page<BugReportPreviewDto> foundBugReports = bugReportService.findAll(0, 3);
        List<BugReportPreviewDto> bugReports = foundBugReports.getContent();

        // then
        Assertions.assertNotNull(foundBugReports);
        Assertions.assertEquals(saved.size(), bugReports.size());

        Assertions.assertEquals(saved.get(0).getUserId(), bugReports.get(0).getUserId());
        Assertions.assertEquals(saved.get(0).getMessage(), bugReports.get(0).getMessage());
        Assertions.assertEquals(saved.get(0).getStatus(), bugReports.get(0).getStatus());

    }

    @Test
    void create_happyPath() {
        // given
        BugReportCreateDto createDto = generateBugReportCreateDto();

        // when
        BugReportFullDto saved = bugReportService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(saved.getCreateTime());
        Assertions.assertNotNull(saved.getStatus());
        Assertions.assertEquals(saved.getStatus(), BugReportEntityStatus.NEW);
        Assertions.assertEquals(createDto.getMessage(), saved.getMessage());
        Assertions.assertEquals(createDto.getImageUrl(), saved.getImageUrl());
        Assertions.assertEquals(createDto.getUserId(), saved.getUserId());
    }

    @Test
    void update_happyPath() {
        // given
        BugReportCreateDto createDto = generateBugReportCreateDto();

        BugReportFullDto saved = bugReportService.create(createDto);

        BugReportUpdateDto updateDto = generateBugReportUpdateDto();
        updateDto.setId(saved.getId());

        // when
        BugReportFullDto updated = bugReportService.update(updateDto);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertNotEquals(saved.getImageUrl(), updated.getImageUrl());
        Assertions.assertNotEquals(saved.getMessage(), updated.getMessage());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
    }

    @Test
    void deleteById_happyPath() {
        // given
        BugReportFullDto bugReportSaved = bugReportService.create(generateBugReportCreateDto());

        // when
        bugReportService.deleteById(bugReportSaved.getId());

        Exception exception = assertThrows(BugReportNotFoundException.class,
                () -> bugReportService.findById(bugReportSaved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(bugReportSaved.getId())));
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(BugReportNotFoundException.class,
                () -> bugReportService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound() {
        // given
        BugReportUpdateDto updateDto = generateBugReportUpdateDto();

        Integer notExistingId = 100;
        updateDto.setId(notExistingId);

        // when
        Exception exception = Assertions.assertThrows(BugReportNotFoundException.class,
                () -> bugReportService.update(updateDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(updateDto.getId())));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(BugReportNotFoundException.class,
                () -> bugReportService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }


}
