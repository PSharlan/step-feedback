package by.itstep.feedback.integration;

import by.itstep.feedback.StepFeedbackApplication;
import by.itstep.feedback.integration.initalizer.MySqlInitializer;
import by.itstep.feedback.mapper.BugReportCommentMapper;
import by.itstep.feedback.repository.BugReportCommentRepository;
import by.itstep.feedback.repository.BugReportRepository;
import by.itstep.feedback.repository.FeedbackCommentRepository;
import by.itstep.feedback.repository.FeedbackRepository;
import by.itstep.feedback.service.BugReportCommentService;
import by.itstep.feedback.service.BugReportService;
import by.itstep.feedback.service.FeedbackCommentService;
import by.itstep.feedback.service.FeedbackService;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.junit.jupiter.Testcontainers;
import com.fasterxml.jackson.databind.ObjectMapper;

@ContextConfiguration(initializers = MySqlInitializer.class,
        classes = StepFeedbackApplication.class)
@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
public class AbstractIntegrationTest {

    @Autowired
    protected BugReportCommentRepository bugReportCommentRepository;

    @Autowired
    protected BugReportRepository bugReportRepository;

    @Autowired
    protected FeedbackCommentRepository feedbackCommentRepository;

    @Autowired
    protected FeedbackRepository feedbackRepository;

    @Autowired
    protected BugReportCommentService bugReportCommentService;

    @Autowired
    protected BugReportService bugReportService;

    @Autowired
    protected FeedbackCommentService feedbackCommentService;

    @Autowired
    protected FeedbackService feedbackService;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected MockMvc mockMvc;

    @BeforeEach
    public void prepareDatabase() {
        bugReportCommentRepository.deleteAll();
        bugReportRepository.deleteAll();
        feedbackCommentRepository.deleteAll();
        feedbackRepository.deleteAll();
    }
}
