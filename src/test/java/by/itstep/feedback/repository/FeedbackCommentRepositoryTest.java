package by.itstep.feedback.repository;

import by.itstep.feedback.entity.FeedbackCommentEntity;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;

public class FeedbackCommentRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        FeedbackCommentEntity toSave = generateFeedbackComment();
        FeedbackEntity feedback = generateFeedback();

        feedbackRepository.save(feedback);
        toSave.setFeedback(feedback);

        FeedbackCommentEntity saved = feedbackCommentRepository.save(toSave);

        // when
        FeedbackCommentEntity found = feedbackCommentRepository.findOneById(saved.getId());

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(),found.getId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<FeedbackCommentEntity> toSave = generateFeedbackComments();
        FeedbackEntity feedback = generateFeedback();

        feedbackRepository.save(feedback);

        for(FeedbackCommentEntity entity : toSave) {
            entity.setFeedback(feedback);
        }

        List<FeedbackCommentEntity> saved = feedbackCommentRepository.saveAll(toSave);

        // when
        List<FeedbackCommentEntity> found = feedbackCommentRepository.findAll();

        //then
        Assertions.assertEquals(found.size(), 3);
        Assertions.assertEquals(found.size(),saved.size());
    }

    @Test
    void create_happyPath() {
        // given
        FeedbackCommentEntity toSave = generateFeedbackComment();
        FeedbackEntity feedback = generateFeedback();

        feedbackRepository.save(feedback);
        toSave.setFeedback(feedback);

        // when
        FeedbackCommentEntity saved = feedbackCommentRepository.save(toSave);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update_happyPath() {
        // given
        FeedbackCommentEntity toSave = generateFeedbackComment();
        FeedbackEntity feedback = generateFeedback();

        feedbackRepository.save(feedback);
        toSave.setFeedback(feedback);

        FeedbackCommentEntity saved = feedbackCommentRepository.save(toSave);
        FeedbackCommentEntity found = feedbackCommentRepository.findOneById(saved.getId());

        // when
        saved.setMessage("hello");
        feedbackCommentRepository.save(toSave);
        FeedbackCommentEntity updated = feedbackCommentRepository.findOneById(saved.getId());

        // then
        Assertions.assertEquals(found.getId(), updated.getId());
        Assertions.assertNotEquals(found.getMessage(), updated.getMessage());
        Assertions.assertNotEquals(found, updated);
    }


    @Test
    void delete_happyPath() {
        // given
        FeedbackCommentEntity toSave = generateFeedbackComment();
        FeedbackEntity feedback = generateFeedback();

        feedbackRepository.save(feedback);
        toSave.setFeedback(feedback);

        FeedbackCommentEntity saved = feedbackCommentRepository.save(toSave);

        // when
        feedbackCommentRepository.deleteById(saved.getId());
        FeedbackCommentEntity foundDeleted = feedbackCommentRepository.findOneById(saved.getId());

        // then
        Assertions.assertNull(foundDeleted);
    }

}
