package by.itstep.feedback.repository;

import by.itstep.feedback.entity.BugReportCommentEntity;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;

public class BugReportCommentRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        BugReportCommentEntity toSave = generateBugReportComment();
        BugReportEntity bugReport = generateBugReport();

        bugReportRepository.save(bugReport);
        toSave.setBugReport(bugReport);

        BugReportCommentEntity saved = bugReportCommentRepository.save(toSave);

        // when
        BugReportCommentEntity found = bugReportCommentRepository.findOneById(saved.getId());

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<BugReportCommentEntity> toSave = generateBugReportComments();
        BugReportEntity bugReport = generateBugReport();

        bugReportRepository.save(bugReport);

        for (BugReportCommentEntity entity : toSave) {
            entity.setBugReport(bugReport);
        }

        List<BugReportCommentEntity> saved = bugReportCommentRepository.saveAll(toSave);

        // when
        List<BugReportCommentEntity> found = bugReportCommentRepository.findAll();

        // then
        Assertions.assertEquals(found.size(), 3);
        Assertions.assertEquals(saved.size(), found.size());
    }

    @Test
    void create_happyPath() {
        // given
        BugReportCommentEntity toSave = generateBugReportComment();
        BugReportEntity bugReport = generateBugReport();

        bugReportRepository.save(bugReport);
        toSave.setBugReport(bugReport);

        // when
        BugReportCommentEntity saved = bugReportCommentRepository.save(toSave);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update_happyPath() {
        // given
        BugReportCommentEntity toSave = generateBugReportComment();
        BugReportEntity bugReport = generateBugReport();

        bugReportRepository.save(bugReport);
        toSave.setBugReport(bugReport);

        BugReportCommentEntity saved = bugReportCommentRepository.save(toSave);
        BugReportCommentEntity found = bugReportCommentRepository.findOneById(saved.getId());

        // when
        saved.setMessage("hello");
        bugReportCommentRepository.save(toSave);
        BugReportCommentEntity updated = bugReportCommentRepository.findOneById(saved.getId());

        // then
        Assertions.assertEquals(found.getId(), updated.getId());
        Assertions.assertNotEquals(found.getMessage(), updated.getMessage());
        Assertions.assertNotEquals(found, updated);
    }

    @Test
    void delete_happyPath() {
        // given
        BugReportCommentEntity toSave = generateBugReportComment();
        BugReportEntity bugReport = generateBugReport();

        bugReportRepository.save(bugReport);
        toSave.setBugReport(bugReport);

        BugReportCommentEntity saved = bugReportCommentRepository.save(toSave);

        // when
        bugReportCommentRepository.deleteById(saved.getId());
        bugReportCommentRepository.flush();
        BugReportCommentEntity foundDeleted = bugReportCommentRepository.findOneById(saved.getId());

        // then
        Assertions.assertNull(foundDeleted);
    }
}