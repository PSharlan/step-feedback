package by.itstep.feedback.repository;

import by.itstep.feedback.entity.FeedbackCommentEntity;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;

public class FeedbackRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        FeedbackEntity toSave = generateFeedback();
        List<FeedbackCommentEntity> feedbackComments = generateFeedbackComments();

        toSave.setComments(feedbackComments);
        for (FeedbackCommentEntity entity : feedbackComments) {
            entity.setFeedback(toSave);
        }

        FeedbackEntity saved = feedbackRepository.save(toSave);
        feedbackCommentRepository.saveAll(feedbackComments);

        // when
        FeedbackEntity found = feedbackRepository.findOneById(saved.getId());

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<FeedbackEntity> toSave = generateFeedbacks();

        List<FeedbackEntity> saved = feedbackRepository.saveAll(toSave);

        // when
        List<FeedbackEntity> found = feedbackRepository.findAll();

        // then
        Assertions.assertEquals(found.size(),3);
        Assertions.assertEquals(found.size(),saved.size());
    }

    @Test
    void save_happyPath() {
        // given
        FeedbackEntity toSave = generateFeedback();

        // when
        FeedbackEntity saved = feedbackRepository.save(toSave);
        FeedbackEntity found = feedbackRepository.findOneById(saved.getId());

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void update_happyPath() {
        // given
        FeedbackEntity toSave = generateFeedback();
        FeedbackEntity saved = feedbackRepository.save(toSave);
        FeedbackEntity found = feedbackRepository.findOneById(saved.getId());

        // when
        saved.setMessage("hello");
        feedbackRepository.save(toSave);
        FeedbackEntity updated = feedbackRepository.findOneById(saved.getId());

        // then
        Assertions.assertEquals(found.getId(), updated.getId());
        Assertions.assertNotEquals(found.getMessage(), updated.getMessage());
        Assertions.assertNotEquals(found, updated);
    }

    @Test
    void delete_happyPath() {
        // given
        FeedbackEntity toSave = generateFeedback();
        FeedbackEntity saved = feedbackRepository.save(toSave);

        // when
        feedbackRepository.deleteById(saved.getId());
        FeedbackEntity foundDeleted = feedbackRepository.findOneById(saved.getId());

        // then
        Assertions.assertNull(foundDeleted);
    }

}
