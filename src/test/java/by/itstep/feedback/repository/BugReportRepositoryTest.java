package by.itstep.feedback.repository;

import by.itstep.feedback.entity.BugReportCommentEntity;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;

public class BugReportRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        BugReportEntity toSave = generateBugReport();
        List<BugReportCommentEntity> bugReportComments = generateBugReportComments();

        toSave.setComments(bugReportComments);
        for (BugReportCommentEntity entity : bugReportComments) {
            entity.setBugReport(toSave);
        }

        BugReportEntity saved = bugReportRepository.save(toSave);
        bugReportCommentRepository.saveAll(bugReportComments);

        // when
        BugReportEntity found = bugReportRepository.findOneById(saved.getId());

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<BugReportEntity> toSave = generateBugReports();

        List<BugReportEntity> saved = bugReportRepository.saveAll(toSave);

        // when
        List<BugReportEntity> found = bugReportRepository.findAll();

        // then
        Assertions.assertEquals(found.size(), 3);
        Assertions.assertEquals(saved.size(), found.size());
    }

    @Test
    void save_happyPath() {
        // given
        BugReportEntity toSave = generateBugReport();

        // when
        BugReportEntity saved = bugReportRepository.save(toSave);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(toSave, saved);
    }

    @Test
    void update_happyPath() {
        // given
        BugReportEntity toSave = generateBugReport();
        BugReportEntity saved = bugReportRepository.save(toSave);
        BugReportEntity found = bugReportRepository.findOneById(saved.getId());

        // when
        saved.setMessage("hello");
        bugReportRepository.save(toSave);
        BugReportEntity updated = bugReportRepository.findOneById(saved.getId());

        // then
        Assertions.assertEquals(found.getId(), updated.getId());
        Assertions.assertNotEquals(found.getMessage(), updated.getMessage());
        Assertions.assertNotEquals(found, updated);
    }

    @Test
    void delete_happyPath() {
        // given
        BugReportEntity toSave = generateBugReport();
        BugReportEntity saved = bugReportRepository.save(toSave);

        // when
        bugReportRepository.deleteById(saved.getId());
        BugReportEntity foundDeleted = bugReportRepository.findOneById(saved.getId());

        // then
        Assertions.assertNull(foundDeleted);
    }

}