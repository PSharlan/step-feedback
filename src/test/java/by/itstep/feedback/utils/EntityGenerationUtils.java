package by.itstep.feedback.repository;

import by.itstep.feedback.entity.BugReportCommentEntity;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.entity.FeedbackCommentEntity;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.entity.enums.BugReportEntityStatus;
import by.itstep.feedback.entity.enums.FeedbackEntityStatus;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

public class EntityGenerationUtils {

    public static FeedbackEntity generateFeedback() {
        FeedbackEntity entity = new FeedbackEntity();
        entity.setMessage("hey");
        entity.setCreateTime(Instant.now());
        entity.setStatus(FeedbackEntityStatus.NEW);
        entity.setUserId(1);

        return entity;
    }

    public static List<FeedbackEntity> generateFeedbacks() {

        FeedbackEntity entity = new FeedbackEntity();
        entity.setMessage("hey1");
        entity.setCreateTime(Instant.now());
        entity.setStatus(FeedbackEntityStatus.NEW);
        entity.setUserId(1);

        FeedbackEntity entity1 = new FeedbackEntity();
        entity1.setMessage("hey2");
        entity1.setCreateTime(Instant.now());
        entity1.setStatus(FeedbackEntityStatus.NEW);
        entity1.setUserId(1);

        FeedbackEntity entity2 = new FeedbackEntity();
        entity2.setMessage("hey3");
        entity2.setCreateTime(Instant.now());
        entity2.setStatus(FeedbackEntityStatus.NEW);
        entity2.setUserId(1);

        return Arrays.asList(entity,entity1,entity2);
    }

    public static FeedbackCommentEntity generateFeedbackComment() {

        FeedbackCommentEntity entity = new FeedbackCommentEntity();
        entity.setMessage("hey");
        entity.setCreateTime(Instant.now());
        entity.setUserId(1);

        return entity;
    }

    public static List<FeedbackCommentEntity> generateFeedbackComments() {

        FeedbackCommentEntity entity = new FeedbackCommentEntity();
        entity.setMessage("hey1");
        entity.setCreateTime(Instant.now());
        entity.setUserId(1);

        FeedbackCommentEntity entity1 = new FeedbackCommentEntity();
        entity1.setMessage("hey2");
        entity1.setCreateTime(Instant.now());
        entity1.setUserId(1);

        FeedbackCommentEntity entity2 = new FeedbackCommentEntity();
        entity2.setMessage("hey3");
        entity2.setCreateTime(Instant.now());
        entity2.setUserId(1);

        return Arrays.asList(entity,entity1,entity2);
    }

    public static BugReportEntity generateBugReport() {
        BugReportEntity entity = new BugReportEntity();
        entity.setMessage("hey");
        entity.setCreateTime(Instant.now());
        entity.setStatus(BugReportEntityStatus.NEW);
        entity.setUserId(1);

        return entity;
    }

    public static  List<BugReportEntity> generateBugReports() {

        BugReportEntity entity = new BugReportEntity();
        entity.setMessage("hey1");
        entity.setCreateTime(Instant.now());
        entity.setStatus(BugReportEntityStatus.NEW);
        entity.setUserId(1);

        BugReportEntity entity1 = new BugReportEntity();
        entity1.setMessage("hey2");
        entity1.setCreateTime(Instant.now());
        entity1.setStatus(BugReportEntityStatus.NEW);
        entity1.setUserId(1);

        BugReportEntity entity2 = new BugReportEntity();
        entity2.setMessage("hey3");
        entity2.setCreateTime(Instant.now());
        entity2.setStatus(BugReportEntityStatus.NEW);
        entity2.setUserId(1);

        return Arrays.asList(entity,entity1,entity2);

    }

    public static BugReportCommentEntity generateBugReportComment() {
        BugReportCommentEntity entity = new BugReportCommentEntity();
        entity.setMessage("hey");
        entity.setCreateTime(Instant.now());
        entity.setUserId(1);

        return entity;
    }

    public static List<BugReportCommentEntity> generateBugReportComments() {

        BugReportCommentEntity entity = new BugReportCommentEntity();
        entity.setMessage("hey1");
        entity.setCreateTime(Instant.now());
        entity.setUserId(1);

        BugReportCommentEntity entity1 = new BugReportCommentEntity();
        entity1.setMessage("hey2");
        entity1.setCreateTime(Instant.now());
        entity1.setUserId(1);

        BugReportCommentEntity entity2 = new BugReportCommentEntity();
        entity2.setMessage("hey3");
        entity2.setCreateTime(Instant.now());
        entity2.setUserId(1);

        return Arrays.asList(entity,entity1,entity2);
    }
}
