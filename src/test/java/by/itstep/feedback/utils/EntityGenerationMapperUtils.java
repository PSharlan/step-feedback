package by.itstep.feedback.utils;

import by.itstep.feedback.dto.bugReport.BugReportCreateDto;
import by.itstep.feedback.dto.bugReport.BugReportUpdateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentCreateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentUpdateDto;
import by.itstep.feedback.dto.feedback.FeedbackCreateDto;
import by.itstep.feedback.dto.feedback.FeedbackUpdateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentCreateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentUpdateDto;
import by.itstep.feedback.entity.BugReportCommentEntity;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.entity.FeedbackCommentEntity;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.entity.enums.BugReportEntityStatus;
import by.itstep.feedback.entity.enums.FeedbackEntityStatus;
import java.time.Instant;

public class EntityGenerationMapperUtils {

    public static BugReportCommentEntity generateBugReportCommentWithId() {
        BugReportCommentEntity bugReportComment = new BugReportCommentEntity();

        bugReportComment.setId(1);
        bugReportComment.setMessage("qwe");
        bugReportComment.setCreateTime(Instant.now());
        bugReportComment.setImageUrl("http://pic.jpg");
        bugReportComment.setUserId(2);

        return bugReportComment;
    }

    public static BugReportEntity generateBugReportWithId() {
        BugReportEntity bugReport = new BugReportEntity();

        bugReport.setId(1);
        bugReport.setMessage("zxc");
        bugReport.setImageUrl("http://pic.jpg");
        bugReport.setCreateTime(Instant.now());
        bugReport.setStatus(BugReportEntityStatus.NEW);
        bugReport.setUserId(2);

        return bugReport;
    }

    public static FeedbackCommentEntity generateFeedbackCommentWithId() {
        FeedbackCommentEntity feedbackComment = new FeedbackCommentEntity();

        feedbackComment.setId(1);
        feedbackComment.setMessage("message");
        feedbackComment.setCreateTime(Instant.now());
        feedbackComment.setImageUrl("http://image");
        feedbackComment.setUserId(2);

        return feedbackComment;
    }

    public static FeedbackEntity generateFeedbackWithId() {
        FeedbackEntity feedback = new FeedbackEntity();

        feedback.setId(1);
        feedback.setMessage("mess");
        feedback.setStatus(FeedbackEntityStatus.NEW);
        feedback.setCreateTime(Instant.now());
        feedback.setUserId(2);

        return feedback;
    }

    public static BugReportCreateDto generateBugReportCreateDto() {
        BugReportCreateDto bugReportCreateDto = new BugReportCreateDto();

        bugReportCreateDto.setMessage("zxc");
        bugReportCreateDto.setImageUrl("qwe");
        bugReportCreateDto.setUserId(1);

        return bugReportCreateDto;
    }

    public static BugReportCommentCreateDto generateBugReportCommentCreateDto() {
        BugReportCommentCreateDto bugReportCommentCreateDto = new BugReportCommentCreateDto();

        bugReportCommentCreateDto.setMessage("zxc");
        bugReportCommentCreateDto.setImageUrl("http://pic.jpg");
        bugReportCommentCreateDto.setUserId(2);

        return bugReportCommentCreateDto;
    }

    public static FeedbackCreateDto generateFeedbackCreateDto() {
        FeedbackCreateDto feedback = new FeedbackCreateDto();

        feedback.setMessage("mess");
        feedback.setUserId(3);

        return feedback;
    }

    public static FeedbackUpdateDto generateFeedbackUpdateDto() {
        FeedbackUpdateDto updateDto = new FeedbackUpdateDto();

        updateDto.setMessage("asd");

        return updateDto;
    }



    public static FeedbackCommentCreateDto generateFeedbackCommentCreateDto() {
        FeedbackCommentCreateDto createDto = new FeedbackCommentCreateDto();

        createDto.setMessage("zxc");
        createDto.setImageUrl("qwe");
        createDto.setUserId(1);

        return createDto;
    }

    public static FeedbackCommentUpdateDto generateFeedbackCommentUpdateDto() {
        FeedbackCommentUpdateDto updateDto = new FeedbackCommentUpdateDto();

        updateDto.setMessage("qwe");
        updateDto.setImageUrl("asd");

        return updateDto;
    }

    public static BugReportUpdateDto generateBugReportUpdateDto() {
        BugReportUpdateDto updateDto = new BugReportUpdateDto();

        updateDto.setMessage("asd");
        updateDto.setImageUrl("zxc");

        return updateDto;
    }

    public static BugReportCommentUpdateDto generateBugReportCommentUpdateDto() {
        BugReportCommentUpdateDto updateDto = new BugReportCommentUpdateDto();

        updateDto.setMessage("qq");
        updateDto.setImageUrl("zz");

        return updateDto;
    }

}
