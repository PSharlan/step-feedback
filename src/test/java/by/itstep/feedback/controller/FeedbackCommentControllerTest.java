package by.itstep.feedback.controller;

import by.itstep.feedback.dto.feedbackComment.FeedbackCommentCreateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentFullDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentUpdateDto;
import by.itstep.feedback.entity.FeedbackCommentEntity;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import by.itstep.feedback.repository.FeedbackCommentRepository;
import by.itstep.feedback.repository.FeedbackRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;
import static by.itstep.feedback.repository.EntityGenerationUtils.generateFeedback;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackCommentCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackCommentUpdateDto;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class FeedbackCommentControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        FeedbackCommentEntity toSave = generateFeedbackCommentWithFeedback();

        FeedbackCommentEntity saved = feedbackCommentRepository.save(toSave);

        // when
        MvcResult result = mockMvc.perform(get("/feedbackComments/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        FeedbackCommentFullDto foundDto = objectMapper.readValue(bytes, FeedbackCommentFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getMessage(), foundDto.getMessage());
        Assertions.assertEquals(saved.getImageUrl(), foundDto.getImageUrl());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/feedbackComments/{id}", notExistingId))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<FeedbackCommentEntity> saved = feedbackCommentRepository.saveAll(generateFeedbackCommentsWithFeedback());

        // when
        MvcResult result = mockMvc.perform(get("/feedbackComments?page=0&size=3"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDtoWithFeedback();

        // when
        MvcResult result = mockMvc.perform(post("/feedbackComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        FeedbackCommentFullDto saved = objectMapper.readValue(bytes, FeedbackCommentFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getMessage(), createDto.getMessage());
        Assertions.assertEquals(saved.getImageUrl(), createDto.getImageUrl());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
    }

    @Test
    void update_happyPath() throws Exception {
        // given
        FeedbackCommentEntity toSave = generateFeedbackCommentWithFeedback();

        FeedbackCommentEntity saved = feedbackCommentRepository.save(toSave);

        FeedbackCommentUpdateDto update = generateFeedbackCommentUpdateDto();
        update.setId(saved.getId());

        // when
        MvcResult result = mockMvc.perform(put("/feedbackComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(update)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        FeedbackCommentFullDto updated = objectMapper.readValue(bytes, FeedbackCommentFullDto.class);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
        Assertions.assertNotEquals(saved.getImageUrl(), updated.getImageUrl());
        Assertions.assertNotEquals(saved.getMessage(), updated.getMessage());
    }

    @Test
    void update_whenNotFound() throws Exception {
        // given
        FeedbackCommentUpdateDto updateDto = generateFeedbackCommentUpdateDto();
        updateDto.setId(0);

        // when
        mockMvc.perform(put("/feedbacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        FeedbackCommentEntity toSave = generateFeedbackCommentWithFeedback();

        FeedbackCommentEntity saved = feedbackCommentRepository.save(toSave);

        // when
        mockMvc.perform(delete("/feedbackComments/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/feedbackComments/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/feedbackComments/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void create_whenMessageEmpty() throws Exception {
        // given
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDtoWithFeedback();
        createDto.setMessage("");

        // when
        mockMvc.perform(post("/feedbackComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
        // then
    }

    @Test
    void create_whenFeedbackIdNull() throws Exception {
        // given
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDtoWithFeedback();
        createDto.setFeedbackId(null);

        // when
        mockMvc.perform(post("/feedbackComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
        // then
    }

    @Test
    void create_whenUserIdNull() throws Exception {
        // given
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDtoWithFeedback();
        createDto.setUserId(null);

        // when
        mockMvc.perform(post("/feedbackComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
        // then
    }

    @Test
    void update_whenMessageEmpty() throws Exception {
        // given
        FeedbackCommentUpdateDto updateDto = generateFeedbackCommentUpdateDto();
        updateDto.setMessage("");

        // when
        mockMvc.perform(post("/feedbackComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());
        // then
    }

    public FeedbackCommentEntity generateFeedbackCommentWithFeedback() {
        FeedbackCommentEntity feedbackComment = generateFeedbackComment();

        feedbackComment.setFeedback(feedbackRepository.save(generateFeedback()));

        return feedbackComment;
    }

    public List<FeedbackCommentEntity> generateFeedbackCommentsWithFeedback() {
        List<FeedbackCommentEntity> toSave = generateFeedbackComments();

        for (FeedbackCommentEntity entity : toSave) {
            entity.setFeedback(feedbackRepository.save(generateFeedback()));
        }

        return toSave;
    }

    public FeedbackCommentCreateDto generateFeedbackCommentCreateDtoWithFeedback() {
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDto();

        FeedbackEntity feedback = feedbackRepository.save(generateFeedback());

        createDto.setFeedbackId(feedback.getId());

        return createDto;
    }
}
