package by.itstep.feedback.controller;

import by.itstep.feedback.dto.bugReport.BugReportCreateDto;
import by.itstep.feedback.dto.bugReport.BugReportFullDto;
import by.itstep.feedback.dto.bugReport.BugReportUpdateDto;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import by.itstep.feedback.repository.BugReportRepository;
import by.itstep.feedback.repository.EntityGenerationUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportUpdateDto;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BugReportControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        BugReportEntity toSave = generateBugReport();

        BugReportEntity saved = bugReportRepository.save(toSave);

        // when
        MvcResult result = mockMvc.perform(get("/bugReports/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte [] bytes = result.getResponse().getContentAsByteArray();
        BugReportFullDto foundDto = objectMapper.readValue(bytes, BugReportFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getMessage(), foundDto.getMessage());
        Assertions.assertEquals(saved.getImageUrl(), foundDto.getImageUrl());
        Assertions.assertEquals(saved.getStatus(), foundDto.getStatus());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/bugReports/{id}", notExistingId))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<BugReportEntity> saved = bugReportRepository.saveAll(generateBugReports());


        // when
        MvcResult result = mockMvc.perform(get("/bugReports?page=0&size=3"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        BugReportCreateDto createDto = generateBugReportCreateDto();

        // when
        MvcResult result = mockMvc.perform(post("/bugReports")
                        .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        BugReportFullDto saved = objectMapper.readValue(bytes, BugReportFullDto.class);


        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getMessage(), createDto.getMessage());
        Assertions.assertEquals(saved.getImageUrl(), createDto.getImageUrl());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
    }

    @Test
    void update_happyPath() throws Exception {
        // given
        BugReportEntity saved = bugReportRepository.save(generateBugReport());

        BugReportUpdateDto update = generateBugReportUpdateDto();
        update.setId(saved.getId());

        // when
        MvcResult result = mockMvc.perform(put("/bugReports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(update)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        BugReportFullDto updated = objectMapper.readValue(bytes, BugReportFullDto.class);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
        Assertions.assertNotEquals(saved.getImageUrl(), updated.getImageUrl());
        Assertions.assertNotEquals(saved.getMessage(), updated.getMessage());
    }

    @Test
    void update_whenNotFound() throws Exception {
        // given
        BugReportUpdateDto updateDto = generateBugReportUpdateDto();
        updateDto.setId(0);

        // when
        mockMvc.perform(put("/bugReports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        BugReportEntity saved = bugReportRepository.save(generateBugReport());

        // when
        mockMvc.perform(delete("/bugReports/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/bugReports/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/bugReports/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void create_whenMessageEmpty() throws Exception {
        // given
        BugReportCreateDto createDto = generateBugReportCreateDto();
        createDto.setMessage("");

        // when
        mockMvc.perform(post("/bugReports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenUserIdNull() throws Exception {
        // given
        BugReportCreateDto createDto = generateBugReportCreateDto();
        createDto.setUserId(null);

        // when
        mockMvc.perform(post("/bugReports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void update_whenMessageEmpty() throws Exception {
        // given
        BugReportUpdateDto updateDto = generateBugReportUpdateDto();
        updateDto.setMessage("");

        // when
        mockMvc.perform(put("/bugReportComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());
        // then
    }
}
