package by.itstep.feedback.controller;

import by.itstep.feedback.dto.feedback.FeedbackCreateDto;
import by.itstep.feedback.dto.feedback.FeedbackFullDto;
import by.itstep.feedback.dto.feedback.FeedbackUpdateDto;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import by.itstep.feedback.repository.FeedbackRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackUpdateDto;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class FeedbackControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        FeedbackEntity toSave = generateFeedback();

        FeedbackEntity saved = feedbackRepository.save(generateFeedback());

        // when
        MvcResult result = mockMvc.perform(get("/feedbacks/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte [] bytes = result.getResponse().getContentAsByteArray();
        FeedbackFullDto foundDto = objectMapper.readValue(bytes, FeedbackFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getMessage(), foundDto.getMessage());
        Assertions.assertEquals(saved.getStatus(), foundDto.getStatus());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/feedbacks/{id}", notExistingId))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<FeedbackEntity> saved = feedbackRepository.saveAll(generateFeedbacks());


        // when
        MvcResult result = mockMvc.perform(get("/feedbacks?page=0&size=3"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        FeedbackCreateDto createDto = generateFeedbackCreateDto();

        // when
        MvcResult result = mockMvc.perform(post("/feedbacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        FeedbackFullDto saved = objectMapper.readValue(bytes, FeedbackFullDto.class);


        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getMessage(), createDto.getMessage());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
    }

    @Test
    void update_happyPath() throws Exception {
        // given
        FeedbackEntity saved = feedbackRepository.save(generateFeedback());

        FeedbackUpdateDto update = generateFeedbackUpdateDto();
        update.setId(saved.getId());

        // when
        MvcResult result = mockMvc.perform(put("/feedbacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(update)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        FeedbackFullDto updated = objectMapper.readValue(bytes, FeedbackFullDto.class);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
        Assertions.assertNotEquals(saved.getMessage(), updated.getMessage());
    }

    @Test
    void update_whenNotFound() throws Exception {
        // given
        FeedbackUpdateDto updateDto = generateFeedbackUpdateDto();
        updateDto.setId(0);

        // when
        mockMvc.perform(put("/feedbacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        FeedbackEntity saved = feedbackRepository.save(generateFeedback());

        // when
        mockMvc.perform(delete("/feedbacks/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/feedbacks/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/feedbacks/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void create_whenMessageEmpty() throws Exception {
        // given
        FeedbackCreateDto createDto = generateFeedbackCreateDto();
        createDto.setMessage("");

        // when
        mockMvc.perform(post("/feedbacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenUserIdNull() throws Exception {
        // given
        FeedbackCreateDto createDto = generateFeedbackCreateDto();
        createDto.setUserId(null);

        // when
        mockMvc.perform(post("/feedbacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
        // then
    }

    @Test
    void update_whenMessageEmpty() throws Exception {
        // given
        FeedbackUpdateDto updateDto = generateFeedbackUpdateDto();
        updateDto.setMessage("");

        // when
        mockMvc.perform(post("/feedbacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());
        // then
    }
}
