package by.itstep.feedback.controller;

import by.itstep.feedback.dto.bugReportComment.BugReportCommentCreateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentFullDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentUpdateDto;
import by.itstep.feedback.entity.BugReportCommentEntity;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import by.itstep.feedback.repository.BugReportCommentRepository;
import by.itstep.feedback.repository.BugReportRepository;
import by.itstep.feedback.repository.EntityGenerationUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static by.itstep.feedback.repository.EntityGenerationUtils.*;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportCommentCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportCommentUpdateDto;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BugReportCommentControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        BugReportCommentEntity toSave = generateBugReportCommentWithBugReport();

        BugReportCommentEntity saved = bugReportCommentRepository.save(toSave);

        // when
        MvcResult result = mockMvc.perform(get("/bugReportComments/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        BugReportCommentFullDto foundDto = objectMapper.readValue(bytes, BugReportCommentFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getMessage(), foundDto.getMessage());
        Assertions.assertEquals(saved.getImageUrl(), foundDto.getImageUrl());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/bugReportComments/{id}", notExistingId))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<BugReportCommentEntity> saved = bugReportCommentRepository.saveAll(generateBugReportCommentsWithBugReport());

        // when
        MvcResult result = mockMvc.perform(get("/bugReportComments?page=0&size=3"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDtoWithBugReport();

        // when
        MvcResult result = mockMvc.perform(post("/bugReportComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        BugReportCommentFullDto saved = objectMapper.readValue(bytes, BugReportCommentFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getMessage(), createDto.getMessage());
        Assertions.assertEquals(saved.getImageUrl(), createDto.getImageUrl());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
    }

    @Test
    void update_happyPath() throws Exception {
        // given
        BugReportCommentEntity toSave = generateBugReportCommentWithBugReport();

        BugReportCommentEntity saved = bugReportCommentRepository.save(toSave);

        BugReportCommentUpdateDto update = generateBugReportCommentUpdateDto();
        update.setId(saved.getId());

        // when
        MvcResult result = mockMvc.perform(put("/bugReportComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(update)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        BugReportCommentFullDto updated = objectMapper.readValue(bytes, BugReportCommentFullDto.class);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
        Assertions.assertNotEquals(saved.getImageUrl(), updated.getImageUrl());
        Assertions.assertNotEquals(saved.getMessage(), updated.getMessage());
    }

    @Test
    void update_whenNotFound() throws Exception {
        // given
        BugReportCommentUpdateDto updateDto = generateBugReportCommentUpdateDto();
        updateDto.setId(0);

        // when
        mockMvc.perform(put("/bugReports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        BugReportCommentEntity toSave = generateBugReportCommentWithBugReport();

        BugReportCommentEntity saved = bugReportCommentRepository.save(toSave);

        // when
        mockMvc.perform(delete("/bugReportComments/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/bugReportComments/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/bugReportComments/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void create_whenMessageEmpty() throws Exception {
        // given
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDtoWithBugReport();
        createDto.setMessage("");

        // when
        mockMvc.perform(post("/bugReportComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenUserIdNull() throws Exception {
        // given
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDtoWithBugReport();
        createDto.setUserId(null);

        // when
        mockMvc.perform(post("/bugReportComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenBugReportIdNull() throws Exception {
        // given
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDtoWithBugReport();
        createDto.setBugReportId(null);

        // when
        mockMvc.perform(post("/bugReportComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void update_whenMessageEmpty() throws Exception {
        // given
        BugReportCommentUpdateDto updateDto = generateBugReportCommentUpdateDto();
        updateDto.setMessage("");

        // when
        mockMvc.perform(put("/bugReportComments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());
        // then
    }

    public BugReportCommentEntity generateBugReportCommentWithBugReport() {
        BugReportCommentEntity bugReportComment = generateBugReportComment();

        bugReportComment.setBugReport(bugReportRepository.save(generateBugReport()));

        return bugReportComment;
    }

    public BugReportCommentCreateDto generateBugReportCommentCreateDtoWithBugReport() {
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDto();

        BugReportEntity bugReport = bugReportRepository.save(generateBugReport());

        createDto.setBugReportId(bugReport.getId());

        return createDto;
    }

    public List<BugReportCommentEntity> generateBugReportCommentsWithBugReport() {
        List<BugReportCommentEntity> toSave = generateBugReportComments();

        for (BugReportCommentEntity entity : toSave) {
            entity.setBugReport(bugReportRepository.save(generateBugReport()));
        }

        return toSave;
    }
}
