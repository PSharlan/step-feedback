package by.itstep.feedback.mapper;

import by.itstep.feedback.dto.bugReportComment.BugReportCommentCreateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentFullDto;
import by.itstep.feedback.entity.BugReportCommentEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.feedback.mapper.BugReportCommentMapper.BUG_REPORT_COMMENT_MAPPER;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportCommentCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateBugReportCommentWithId;

public class BugReportCommentMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToEntity_happyPath() {
        // given
        BugReportCommentCreateDto createDto = generateBugReportCommentCreateDto();

        // when
        BugReportCommentEntity entity = BUG_REPORT_COMMENT_MAPPER.mapToEntity(createDto);

        // then
        Assertions.assertNull(entity.getId());
        Assertions.assertEquals(createDto.getMessage(), entity.getMessage());
        Assertions.assertEquals(createDto.getImageUrl(), entity.getImageUrl());
        Assertions.assertEquals(createDto.getUserId(), entity.getUserId());
        Assertions.assertNull(entity.getBugReport());
    }

    @Test
    void mapToFullDto_happyPath() {
        // given
        BugReportCommentEntity bugReportComment = generateBugReportCommentWithId();

        // when
        BugReportCommentFullDto bugReportCommentFullDto = BUG_REPORT_COMMENT_MAPPER.mapToFullDto(bugReportComment);

        // then
        Assertions.assertEquals(bugReportComment.getId(), bugReportCommentFullDto.getId());
        Assertions.assertEquals(bugReportComment.getMessage(), bugReportCommentFullDto.getMessage());
        Assertions.assertEquals(bugReportComment.getCreateTime(), bugReportCommentFullDto.getCreateTime());
        Assertions.assertEquals(bugReportComment.getImageUrl(), bugReportCommentFullDto.getImageUrl());
        Assertions.assertEquals(bugReportComment.getUserId(), bugReportCommentFullDto.getUserId());
    }

    @Test
    void mapToFullDtoList_happyPath() {
        // given
        List<BugReportCommentEntity> entities = new ArrayList<>();

        entities.add(generateBugReportCommentWithId());
        entities.add(generateBugReportCommentWithId());

        // when
        List<BugReportCommentFullDto> fullDtos = BUG_REPORT_COMMENT_MAPPER.mapToFullDtoList(entities);

        // then
        Assertions.assertEquals(entities.size(), fullDtos.size());
        Assertions.assertEquals(entities.get(0).getCreateTime(), fullDtos.get(0).getCreateTime());
        Assertions.assertEquals(entities.get(0).getMessage(), fullDtos.get(0).getMessage());
        Assertions.assertEquals(entities.get(0).getImageUrl(), fullDtos.get(0).getImageUrl());
        Assertions.assertEquals(entities.get(0).getUserId(), fullDtos.get(0).getUserId());
    }


}
