package by.itstep.feedback.mapper;

import by.itstep.feedback.dto.bugReport.BugReportCreateDto;
import by.itstep.feedback.dto.bugReport.BugReportFullDto;
import by.itstep.feedback.dto.bugReport.BugReportPreviewDto;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static by.itstep.feedback.mapper.BugReportMapper.BUG_REPORT_MAPPER;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.*;

public class BugReportMapperTest extends AbstractIntegrationTest{

    @Test
    void mapToEntity_happyPath() {
        // given
        BugReportCreateDto createDto = generateBugReportCreateDto();

        // when
        BugReportEntity entity = BUG_REPORT_MAPPER.mapToEntity(createDto);

        // then
        Assertions.assertNull(entity.getId());
        Assertions.assertEquals(createDto.getMessage(), entity.getMessage());
        Assertions.assertEquals(createDto.getImageUrl(), entity.getImageUrl());
        Assertions.assertEquals(createDto.getUserId(), entity.getUserId());
    }

    @Test
    void mapToFullDto_happyPath() {
        // given
        BugReportEntity bugReport = generateBugReportWithId();
        bugReport.setComments(Collections.singletonList(generateBugReportCommentWithId()));

        // when
        BugReportFullDto bugReportFullDto = BUG_REPORT_MAPPER.mapToFullDto(bugReport);

        // then
        Assertions.assertEquals(bugReport.getId(), bugReportFullDto.getId());
        Assertions.assertEquals(bugReport.getMessage(), bugReportFullDto.getMessage());
        Assertions.assertEquals(bugReport.getImageUrl(), bugReportFullDto.getImageUrl());
        Assertions.assertEquals(bugReport.getCreateTime(), bugReportFullDto.getCreateTime());
        Assertions.assertEquals(bugReport.getStatus(), bugReportFullDto.getStatus());
        Assertions.assertEquals(bugReport.getUserId(), bugReportFullDto.getUserId());
        Assertions.assertEquals(bugReport.getComments().size(), bugReportFullDto.getComments().size());
    }

    @Test
    void mapToPreviewDto_happyPath() {
        // given
        BugReportEntity bugReport = generateBugReportWithId();

        // when
        BugReportPreviewDto previewDto = BUG_REPORT_MAPPER.mapToPreviewDto(bugReport);

        // then
        Assertions.assertEquals(bugReport.getId(), previewDto.getId());
        Assertions.assertEquals(bugReport.getMessage(), previewDto.getMessage());
        Assertions.assertEquals(bugReport.getImageUrl(), previewDto.getImageUrl());
        Assertions.assertEquals(bugReport.getStatus(), previewDto.getStatus());
        Assertions.assertEquals(bugReport.getCreateTime(), previewDto.getCreateTime());
        Assertions.assertEquals(bugReport.getUserId(), previewDto.getUserId());
    }

    @Test
    void mapToPreviewDtoList_happyPath() {
        // given
        List<BugReportEntity> entities = new ArrayList<>();

        entities.add(generateBugReportWithId());
        entities.add(generateBugReportWithId());

        // when
        List<BugReportPreviewDto> previewDtos = BUG_REPORT_MAPPER.mapToPreviewDtoList(entities);

        // then
        Assertions.assertEquals(entities.size(), previewDtos.size());
        Assertions.assertEquals(entities.get(0).getCreateTime(), previewDtos.get(0).getCreateTime());
        Assertions.assertEquals(entities.get(0).getMessage(), previewDtos.get(0).getMessage());
        Assertions.assertEquals(entities.get(0).getImageUrl(), previewDtos.get(0).getImageUrl());
        Assertions.assertEquals(entities.get(0).getStatus(), previewDtos.get(0).getStatus());
        Assertions.assertEquals(entities.get(0).getUserId(), previewDtos.get(0).getUserId());
    }

}
