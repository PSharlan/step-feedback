package by.itstep.feedback.mapper;

import by.itstep.feedback.dto.feedbackComment.FeedbackCommentCreateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentFullDto;
import by.itstep.feedback.entity.FeedbackCommentEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.feedback.mapper.FeedbackCommentMapper.FEEDBACK_COMMENT_MAPPER;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackCommentCreateDto;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.generateFeedbackCommentWithId;

public class FeedBackCommentMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToEntity_happyPath() {
        // given
        FeedbackCommentCreateDto createDto = generateFeedbackCommentCreateDto();

        // when
        FeedbackCommentEntity entity = FEEDBACK_COMMENT_MAPPER.mapToEntity(createDto);

        // then
        Assertions.assertNull(entity.getId());
        Assertions.assertEquals(createDto.getMessage(), entity.getMessage());
        Assertions.assertEquals(createDto.getImageUrl(), entity.getImageUrl());
        Assertions.assertEquals(createDto.getUserId(), entity.getUserId());
        Assertions.assertNull(entity.getFeedback());
    }

    @Test
    void mapToFullDto_happyPath() {
        // given
        FeedbackCommentEntity feedbackComment = generateFeedbackCommentWithId();

        // when
        FeedbackCommentFullDto feedbackCommentFullDto = FEEDBACK_COMMENT_MAPPER.mapToFullDto(feedbackComment);

        // then
        Assertions.assertEquals(feedbackComment.getId(), feedbackCommentFullDto.getId());
        Assertions.assertEquals(feedbackComment.getMessage(), feedbackCommentFullDto.getMessage());
        Assertions.assertEquals(feedbackComment.getCreateTime(), feedbackCommentFullDto.getCreateTime());
        Assertions.assertEquals(feedbackComment.getImageUrl(), feedbackCommentFullDto.getImageUrl());
        Assertions.assertEquals(feedbackComment.getUserId(), feedbackCommentFullDto.getUserId());
    }

    @Test
    void mapToFullDtoList_happyPath() {
        // given
        List<FeedbackCommentEntity> entities = new ArrayList<>();

        entities.add(generateFeedbackCommentWithId());
        entities.add(generateFeedbackCommentWithId());

        // when
        List<FeedbackCommentFullDto> fullDtos = FEEDBACK_COMMENT_MAPPER.mapToFullDtoList(entities);

        // then
        Assertions.assertEquals(entities.size(), fullDtos.size());
        Assertions.assertEquals(entities.get(0).getCreateTime(), fullDtos.get(0).getCreateTime());
        Assertions.assertEquals(entities.get(0).getMessage(), fullDtos.get(0).getMessage());
        Assertions.assertEquals(entities.get(0).getImageUrl(), fullDtos.get(0).getImageUrl());
        Assertions.assertEquals(entities.get(0).getUserId(), fullDtos.get(0).getUserId());
    }

}
