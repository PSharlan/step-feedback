package by.itstep.feedback.mapper;

import by.itstep.feedback.dto.feedback.FeedbackCreateDto;
import by.itstep.feedback.dto.feedback.FeedbackFullDto;
import by.itstep.feedback.dto.feedback.FeedbackPreviewDto;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static by.itstep.feedback.mapper.FeedbackMapper.FEEDBACK_MAPPER;
import static by.itstep.feedback.utils.EntityGenerationMapperUtils.*;

public class FeedbackMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToEntity_happyPath() {
        //given
        FeedbackCreateDto createDto = generateFeedbackCreateDto();

        //when
        FeedbackEntity feedback = FEEDBACK_MAPPER.mapToEntity(createDto);

        //then
        Assertions.assertNull(feedback.getId());
        Assertions.assertEquals(createDto.getMessage(),feedback.getMessage());
        Assertions.assertEquals(createDto.getUserId(), feedback.getUserId());
    }

    @Test
    void mapToFullDto_happyPath() {
        //given
        FeedbackEntity feedback = generateFeedbackWithId();
        feedback.setComments(Collections.singletonList(generateFeedbackCommentWithId()));

        //when
        FeedbackFullDto feedbackFullDto = FEEDBACK_MAPPER.mapToFullDto(feedback);

        //then
        Assertions.assertEquals(feedback.getId(), feedbackFullDto.getId());
        Assertions.assertEquals(feedback.getMessage(), feedbackFullDto.getMessage());
        Assertions.assertEquals(feedback.getCreateTime(), feedbackFullDto.getCreateTime());
        Assertions.assertEquals(feedback.getUserId(), feedbackFullDto.getUserId());
        Assertions.assertEquals(feedback.getStatus(), feedbackFullDto.getStatus());
        Assertions.assertEquals(feedback.getComments().size(), feedbackFullDto.getComments().size());
    }

    @Test
    void mapToPreviewDto_happyPath() {
        // given
        FeedbackEntity feedback = generateFeedbackWithId();

        // when
        FeedbackPreviewDto previewDto = FEEDBACK_MAPPER.mapToPreviewDto(feedback);

        // then
        Assertions.assertEquals(feedback.getId(), previewDto.getId());
        Assertions.assertEquals(feedback.getMessage(), previewDto.getMessage());
        Assertions.assertEquals(feedback.getCreateTime(), previewDto.getCreateTime());
        Assertions.assertEquals(feedback.getUserId(), previewDto.getUserId());
    }

    @Test
    void mapToPreviewDtoList_happyPath() {
        // given
        List<FeedbackEntity> entities = new ArrayList<>();

        entities.add(generateFeedbackWithId());
        entities.add(generateFeedbackWithId());

        // when
        List<FeedbackPreviewDto> previewDtos = FEEDBACK_MAPPER.mapToPreviewDtoList(entities);

        // then
        Assertions.assertEquals(entities.size(), previewDtos.size());
        Assertions.assertEquals(entities.get(0).getCreateTime(), previewDtos.get(0).getCreateTime());
        Assertions.assertEquals(entities.get(0).getMessage(), previewDtos.get(0).getMessage());
        Assertions.assertEquals(entities.get(0).getUserId(), previewDtos.get(0).getUserId());
    }


}
