package by.itstep.feedback.entity;

import by.itstep.feedback.entity.enums.FeedbackEntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "feedback")
public class FeedbackEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "create_time", nullable = false)
    private Instant createTime;

    @Column(name = "status", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private FeedbackEntityStatus status;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "feedback", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE})
    private List<FeedbackCommentEntity> comments = new ArrayList<>();

    @Column(name = "user_id", nullable = false)
    private Integer userId;

}
