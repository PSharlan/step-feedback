package by.itstep.feedback.entity.enums;

public enum FeedbackEntityStatus {

    NEW,
    PROCESSED
}
