package by.itstep.feedback.entity.enums;

public enum BugReportEntityStatus {

    NEW,
    PROCESSING,
    CLOSED,
    REJECTED
}
