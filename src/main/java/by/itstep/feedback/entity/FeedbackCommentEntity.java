package by.itstep.feedback.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "feedback_comment")
public class FeedbackCommentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "create_time", nullable = false)
    private Instant createTime;

    @Column(name = "image_url")
    private String imageUrl;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "feedback_id", nullable = false)
    private FeedbackEntity feedback;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

}
