package by.itstep.feedback.entity;

import by.itstep.feedback.entity.enums.BugReportEntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "bug_report")
public class BugReportEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "create_time", nullable = false)
    private Instant createTime;

    @Column(name = "status", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private BugReportEntityStatus status;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "bugReport", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE})
    private List<BugReportCommentEntity> comments = new ArrayList<>();

    @Column(name = "user_id", nullable = false)
    private Integer userId;

}
