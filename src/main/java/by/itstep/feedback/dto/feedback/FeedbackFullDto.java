package by.itstep.feedback.dto.feedback;

import by.itstep.feedback.dto.feedbackComment.FeedbackCommentFullDto;
import by.itstep.feedback.entity.enums.FeedbackEntityStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.Instant;
import java.util.List;

@Data
public class FeedbackFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current feedback")
    private Integer id;

    @ApiModelProperty(notes = "Feedback message")
    private String message;

    @ApiModelProperty(example = "2021-01-01T17:07:00.290Z", notes = "Create time of feedback")
    private Instant createTime;

    @ApiModelProperty(example = "NEW", notes = "Status of feedback")
    private FeedbackEntityStatus status;

    @ApiModelProperty(notes = "List of feedbackComments for feedback")
    private List<FeedbackCommentFullDto> comments;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;

}
