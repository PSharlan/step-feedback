package by.itstep.feedback.dto.feedback;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class FeedbackCreateDto {

    @ApiModelProperty(notes = "Feedback message")
    @NotEmpty(message = "Message can't be empty!")
    private String message;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "userId can't be null")
    private Integer userId;

}
