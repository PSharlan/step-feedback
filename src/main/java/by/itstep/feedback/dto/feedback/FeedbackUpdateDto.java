package by.itstep.feedback.dto.feedback;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class FeedbackUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current feedback")
    @NotNull(message = "feedbackId can't be null")
    private Integer id;

    @ApiModelProperty(notes = "Feedback message")
    @NotEmpty(message = "Message can't be empty!")
    private String message;

}
