package by.itstep.feedback.dto.feedbackComment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.Instant;

@Data
public class FeedbackCommentFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current feedbackComment")
    private Integer id;

    @ApiModelProperty(notes = "FeedbackComment message")
    private String message;

    @ApiModelProperty(example = "2021-01-01T17:07:00.290Z", notes = "Create time of feedbackComment")
    private Instant createTime;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link to the image file")
    private String imageUrl;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;

}
