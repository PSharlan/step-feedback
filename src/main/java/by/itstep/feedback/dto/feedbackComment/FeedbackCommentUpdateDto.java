package by.itstep.feedback.dto.feedbackComment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class FeedbackCommentUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current feedbackComment")
    @NotNull(message = "feedbackCommentId can't be null")
    private Integer id;

    @ApiModelProperty(notes = "FeedbackComment message")
    @NotEmpty(message = "Message can't be empty!")
    private String message;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link to the image file")
    private String imageUrl;

}
