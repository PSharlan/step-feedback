package by.itstep.feedback.dto.bugReport;

import by.itstep.feedback.entity.enums.BugReportEntityStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.Instant;

@Data
public class BugReportPreviewDto {

    @ApiModelProperty(example = "1", notes = "Id of the current bugReport")
    private Integer id;

    @ApiModelProperty(notes = "BugReport message")
    private String message;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link to the image file")
    private String imageUrl;

    @ApiModelProperty(example = "2021-01-01T17:07:00.290Z", notes = "Create time of bugReport")
    private Instant createTime;

    @ApiModelProperty(example = "NEW", notes = "Status of bugReport")
    private BugReportEntityStatus status;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;

}
