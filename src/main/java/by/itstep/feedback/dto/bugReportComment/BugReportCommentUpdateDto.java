package by.itstep.feedback.dto.bugReportComment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BugReportCommentUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current bugReportComment")
    @NotNull(message = "bugReportCommentId can't be null")
    private Integer id;

    @ApiModelProperty(notes = "BugReportComment message")
    @NotEmpty(message = "Message can't be empty!")
    private String message;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link to the image file")
    private String imageUrl;

}
