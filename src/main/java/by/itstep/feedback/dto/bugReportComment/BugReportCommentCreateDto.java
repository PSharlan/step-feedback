package by.itstep.feedback.dto.bugReportComment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BugReportCommentCreateDto {

    @ApiModelProperty(notes = "BugReportComment message")
    @NotEmpty(message = "Message can't be empty!")
    private String message;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link to the image file")
    private String imageUrl;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "userId can't be null!")
    private Integer userId;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "bugReportId can't be null")
    private Integer bugReportId;

}
