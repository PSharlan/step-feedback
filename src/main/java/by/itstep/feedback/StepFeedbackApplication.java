package by.itstep.feedback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepFeedbackApplication {

	public static void main(String[] args) {
		SpringApplication.run(StepFeedbackApplication.class, args);
	}

}
