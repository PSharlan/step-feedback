package by.itstep.feedback.mapper;

import by.itstep.feedback.dto.feedback.FeedbackCreateDto;
import by.itstep.feedback.dto.feedback.FeedbackFullDto;
import by.itstep.feedback.dto.feedback.FeedbackPreviewDto;
import by.itstep.feedback.entity.FeedbackEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = FeedbackCommentMapper.class)
public interface FeedbackMapper {

    FeedbackMapper FEEDBACK_MAPPER = Mappers.getMapper(FeedbackMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "createTime", ignore = true)
    FeedbackEntity mapToEntity(FeedbackCreateDto createDto);

    FeedbackFullDto mapToFullDto(FeedbackEntity entity);

    FeedbackPreviewDto mapToPreviewDto(FeedbackEntity entity);

    List<FeedbackPreviewDto> mapToPreviewDtoList(List<FeedbackEntity> feedbackEntities);

}
