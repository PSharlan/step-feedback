package by.itstep.feedback.mapper;

import by.itstep.feedback.dto.bugReport.BugReportCreateDto;
import by.itstep.feedback.dto.bugReport.BugReportFullDto;
import by.itstep.feedback.dto.bugReport.BugReportPreviewDto;
import by.itstep.feedback.entity.BugReportEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = BugReportCommentMapper.class)
public interface BugReportMapper {

    BugReportMapper BUG_REPORT_MAPPER = Mappers.getMapper(BugReportMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "createTime", ignore = true)
    BugReportEntity mapToEntity(BugReportCreateDto createDto);

    BugReportFullDto mapToFullDto(BugReportEntity entity);

    BugReportPreviewDto mapToPreviewDto(BugReportEntity entity);

    List<BugReportPreviewDto> mapToPreviewDtoList(List<BugReportEntity> bugReportEntities);

}
