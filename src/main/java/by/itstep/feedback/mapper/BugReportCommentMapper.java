package by.itstep.feedback.mapper;

import by.itstep.feedback.dto.bugReportComment.BugReportCommentCreateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentFullDto;
import by.itstep.feedback.entity.BugReportCommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = BugReportMapper.class)
public interface BugReportCommentMapper {

    BugReportCommentMapper BUG_REPORT_COMMENT_MAPPER = Mappers.getMapper(BugReportCommentMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "bugReport", ignore = true)
    BugReportCommentEntity mapToEntity(BugReportCommentCreateDto createDto);

    BugReportCommentFullDto mapToFullDto(BugReportCommentEntity entity);

    List<BugReportCommentFullDto> mapToFullDtoList(List<BugReportCommentEntity> bugReportCommentEntities);

}
