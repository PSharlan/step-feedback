package by.itstep.feedback.mapper;

import by.itstep.feedback.dto.feedbackComment.FeedbackCommentCreateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentFullDto;
import by.itstep.feedback.entity.FeedbackCommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = FeedbackMapper.class)
public interface FeedbackCommentMapper {

    FeedbackCommentMapper FEEDBACK_COMMENT_MAPPER = Mappers.getMapper(FeedbackCommentMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "feedback", ignore = true)
    FeedbackCommentEntity mapToEntity(FeedbackCommentCreateDto createDto);

    FeedbackCommentFullDto mapToFullDto(FeedbackCommentEntity entity);

    List<FeedbackCommentFullDto> mapToFullDtoList(List<FeedbackCommentEntity> feedbackCommentEntities);

}
