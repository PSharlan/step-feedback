package by.itstep.feedback.controller;

import by.itstep.feedback.dto.feedbackComment.FeedbackCommentCreateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentFullDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentUpdateDto;
import by.itstep.feedback.service.FeedbackCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(description = "Controller dedicated to manage feedbackComments")
@RestController
public class FeedbackCommentController {

    @Autowired
    private FeedbackCommentService feedbackCommentService;

    @GetMapping("/feedbackComments/{id}")
    @ApiOperation(value = "Find one feedbackComment by id", notes = "Existing id must be specified")
    public FeedbackCommentFullDto getById(@PathVariable Integer id) {
        return feedbackCommentService.findById(id);
    }

    @GetMapping("/feedbackComments")
    @ApiOperation(value = "Find all feedbackComments")
    public Page<FeedbackCommentFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return feedbackCommentService.findAll(page, size);
    }

    @PostMapping("/feedbackComments")
    @ApiOperation(value = "Create feedbackComment")
    public FeedbackCommentFullDto create(@Valid @RequestBody FeedbackCommentCreateDto createDto) {
        return feedbackCommentService.create(createDto);
    }

    @PutMapping("/feedbackComments")
    @ApiOperation(value = "Update feedbackComment")
    public FeedbackCommentFullDto update(@Valid @RequestBody FeedbackCommentUpdateDto updateDto) {
        return feedbackCommentService.update(updateDto);
    }

    @DeleteMapping("/feedbackComments/{id}")
    @ApiOperation(value = "Delete feedbackComment by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        feedbackCommentService.deleteById(id);
    }

}
