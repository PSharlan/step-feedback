package by.itstep.feedback.controller;

import by.itstep.feedback.dto.bugReport.BugReportCreateDto;
import by.itstep.feedback.dto.bugReport.BugReportFullDto;
import by.itstep.feedback.dto.bugReport.BugReportPreviewDto;
import by.itstep.feedback.dto.bugReport.BugReportUpdateDto;
import by.itstep.feedback.service.BugReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(description = "Controller dedicated to manage bugReports")
@RestController
public class BugReportController {

    @Autowired
    private BugReportService bugReportService;

    @GetMapping("/bugReports/{id}")
    @ApiOperation(value = "Find one bugReport by id", notes = "Existing id must be specified")
    public BugReportFullDto getById(@PathVariable Integer id) {
        return bugReportService.findById(id);
    }

    @GetMapping("/bugReports")
    @ApiOperation(value = "Find all bugReports")
    public Page<BugReportPreviewDto> findAll(@RequestParam int page, @RequestParam int size) {
        return bugReportService.findAll(page, size);
    }

    @PostMapping("/bugReports")
    @ApiOperation(value = "Create bugReport")
    public BugReportFullDto create(@Valid @RequestBody BugReportCreateDto createDto) {
        return bugReportService.create(createDto);
    }

    @PutMapping("/bugReports")
    @ApiOperation(value = "Update bugReport")
    public BugReportFullDto update(@Valid @RequestBody BugReportUpdateDto updateDto) {
        return bugReportService.update(updateDto);
    }

    @DeleteMapping("/bugReports/{id}")
    @ApiOperation(value = "Delete bugReport by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        bugReportService.deleteById(id);
    }

}
