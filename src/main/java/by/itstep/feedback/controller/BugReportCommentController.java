package by.itstep.feedback.controller;

import by.itstep.feedback.dto.bugReportComment.BugReportCommentCreateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentFullDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentUpdateDto;
import by.itstep.feedback.service.BugReportCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(description = "Controller dedicated to manage bugReportComments")
@RestController
public class BugReportCommentController {

    @Autowired
    private BugReportCommentService bugReportCommentService;

    @GetMapping("/bugReportComments/{id}")
    @ApiOperation(value = "Find one bugReportComment by id", notes = "Existing id must be specified")
    public BugReportCommentFullDto getById(@PathVariable Integer id) {
        return bugReportCommentService.findById(id);
    }

    @GetMapping("/bugReportComments")
    @ApiOperation(value = "Find all bugReportComments")
    public Page<BugReportCommentFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return bugReportCommentService.findAll(page, size);
    }

    @PostMapping("/bugReportComments")
    @ApiOperation(value = "Create bugReportComment")
    public BugReportCommentFullDto create(@Valid @RequestBody BugReportCommentCreateDto createDto) {
        return bugReportCommentService.create(createDto);
    }

    @PutMapping("/bugReportComments")
    @ApiOperation(value = "Update bugReportComment")
    public BugReportCommentFullDto update(@Valid @RequestBody BugReportCommentUpdateDto updateDto) {
        return bugReportCommentService.update(updateDto);
    }

    @DeleteMapping("/bugReportComments/{id}")
    @ApiOperation(value = "Delete bugReportComment by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        bugReportCommentService.deleteById(id);
    }

}
