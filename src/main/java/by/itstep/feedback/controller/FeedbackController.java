package by.itstep.feedback.controller;

import by.itstep.feedback.dto.feedback.FeedbackCreateDto;
import by.itstep.feedback.dto.feedback.FeedbackFullDto;
import by.itstep.feedback.dto.feedback.FeedbackPreviewDto;
import by.itstep.feedback.dto.feedback.FeedbackUpdateDto;
import by.itstep.feedback.service.FeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(description = "Controller dedicated to manage feedbacks")
@RestController
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;

    @GetMapping("/feedbacks/{id}")
    @ApiOperation(value = "Find one feedback by id", notes = "Existing id must be specified")
    public FeedbackFullDto getById(@PathVariable Integer id) {
        return feedbackService.findById(id);
    }

    @GetMapping("/feedbacks")
    @ApiOperation(value = "Find all feedbacks")
    public Page<FeedbackPreviewDto> findAll(@RequestParam int page, @RequestParam int size) {
        return feedbackService.findAll(page, size);
    }

    @PostMapping("/feedbacks")
    @ApiOperation(value = "Create feedback")
    public FeedbackFullDto create(@Valid @RequestBody FeedbackCreateDto createDto) {
        return feedbackService.create(createDto);
    }

    @PutMapping("/feedbacks")
    @ApiOperation(value = "Update feedback")
    public FeedbackFullDto update(@Valid @RequestBody FeedbackUpdateDto updateDto) {
        return feedbackService.update(updateDto);
    }

    @DeleteMapping("/feedbacks/{id}")
    @ApiOperation(value = "Delete feedback by id", notes = "Existing id must be specified")
    public void delete (@PathVariable Integer id) {
        feedbackService.deleteById(id);
    }
}
