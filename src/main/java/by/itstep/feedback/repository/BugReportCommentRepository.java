package by.itstep.feedback.repository;
import by.itstep.feedback.entity.BugReportCommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BugReportCommentRepository extends JpaRepository<BugReportCommentEntity, Integer> {

    BugReportCommentEntity findOneById(Integer id);

}
