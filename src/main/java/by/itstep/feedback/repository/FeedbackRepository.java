package by.itstep.feedback.repository;
import by.itstep.feedback.entity.FeedbackEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackRepository extends JpaRepository<FeedbackEntity, Integer> {

    FeedbackEntity findOneById(Integer id);

}
