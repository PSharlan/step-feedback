package by.itstep.feedback.repository;
import by.itstep.feedback.entity.FeedbackCommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackCommentRepository extends JpaRepository<FeedbackCommentEntity, Integer> {

    FeedbackCommentEntity findOneById(Integer id);

}
