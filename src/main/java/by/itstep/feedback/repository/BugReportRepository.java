package by.itstep.feedback.repository;
import by.itstep.feedback.entity.BugReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BugReportRepository extends JpaRepository<BugReportEntity, Integer> {

    BugReportEntity findOneById(Integer id);
    
}
