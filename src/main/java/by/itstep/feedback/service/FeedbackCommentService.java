package by.itstep.feedback.service;

import by.itstep.feedback.dto.feedbackComment.FeedbackCommentCreateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentFullDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentUpdateDto;
import org.springframework.data.domain.Page;

public interface FeedbackCommentService {

    FeedbackCommentFullDto findById(Integer id);

    Page<FeedbackCommentFullDto> findAll(int page, int size);

    FeedbackCommentFullDto create(FeedbackCommentCreateDto createDto);

    FeedbackCommentFullDto update (FeedbackCommentUpdateDto updateDto);

    void deleteById(Integer id);

}
