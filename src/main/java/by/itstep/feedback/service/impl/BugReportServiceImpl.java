package by.itstep.feedback.service.impl;

import by.itstep.feedback.dto.bugReport.BugReportCreateDto;
import by.itstep.feedback.dto.bugReport.BugReportFullDto;
import by.itstep.feedback.dto.bugReport.BugReportPreviewDto;
import by.itstep.feedback.dto.bugReport.BugReportUpdateDto;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.entity.enums.BugReportEntityStatus;
import by.itstep.feedback.exceptions.BugReportNotFoundException;
import by.itstep.feedback.repository.BugReportRepository;
import by.itstep.feedback.service.BugReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

import static by.itstep.feedback.mapper.BugReportMapper.BUG_REPORT_MAPPER;

@Slf4j
@Service
public class BugReportServiceImpl implements BugReportService {

    @Autowired
    private BugReportRepository bugReportRepository;

    @Override
    @Transactional(readOnly = true)
    public BugReportFullDto findById(Integer id) {
        BugReportEntity foundEntity = bugReportRepository.findById(id)
                .orElseThrow(() -> new BugReportNotFoundException(id));

        log.info("BugReportServiceImpl -> found bugReport: {} by id: {}", foundEntity, id);

        return BUG_REPORT_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BugReportPreviewDto> findAll(int page, int size) {
        Page<BugReportEntity> foundEntities = bugReportRepository.findAll(PageRequest.of(page, size));

        Page<BugReportPreviewDto> dto = foundEntities.map(entity -> BUG_REPORT_MAPPER.mapToPreviewDto(entity));

        log.info("BugReportServiceImpl -> found {} bugReports", dto.getNumberOfElements());

        return dto;
    }

    @Override
    @Transactional
    public BugReportFullDto create(BugReportCreateDto createDto) {
        BugReportEntity entityToSave = BUG_REPORT_MAPPER.mapToEntity(createDto);

        entityToSave.setCreateTime(Instant.now());
        entityToSave.setStatus(BugReportEntityStatus.NEW);

        BugReportEntity savedEntity = bugReportRepository.save(entityToSave);

        log.info("BugReportServiceImpl -> bugReport {} successfully saved", savedEntity);

        return BUG_REPORT_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public BugReportFullDto update(BugReportUpdateDto updateDto) {
        BugReportEntity entityToUpdate = bugReportRepository.findById(updateDto.getId())
                .orElseThrow(() -> new BugReportNotFoundException(updateDto.getId()));

        entityToUpdate.setMessage(updateDto.getMessage());
        entityToUpdate.setImageUrl(updateDto.getImageUrl());

        BugReportEntity updatedEntity = bugReportRepository.save(entityToUpdate);

        log.info("BugReportServiceImpl -> bugReport {} successfully updated", updatedEntity);

        return BUG_REPORT_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        bugReportRepository.findById(id)
                .orElseThrow(() -> new BugReportNotFoundException(id));

        bugReportRepository.deleteById(id);

        log.info("BugReportServiceImpl -> bugReport by id: {} successfully deleted", id);
    }
}
