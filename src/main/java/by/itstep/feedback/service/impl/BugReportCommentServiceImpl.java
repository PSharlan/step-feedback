package by.itstep.feedback.service.impl;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentCreateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentFullDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentUpdateDto;
import by.itstep.feedback.entity.BugReportCommentEntity;
import by.itstep.feedback.entity.BugReportEntity;
import by.itstep.feedback.exceptions.BugReportCommentNotFoundException;
import by.itstep.feedback.exceptions.BugReportNotFoundException;
import by.itstep.feedback.repository.BugReportCommentRepository;
import by.itstep.feedback.repository.BugReportRepository;
import by.itstep.feedback.service.BugReportCommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.Instant;

import static by.itstep.feedback.mapper.BugReportCommentMapper.BUG_REPORT_COMMENT_MAPPER;

@Slf4j
@Service
public class BugReportCommentServiceImpl implements BugReportCommentService {

    @Autowired
    private BugReportCommentRepository bugReportCommentRepository;

    @Autowired
    private BugReportRepository bugReportRepository;

    @Override
    @Transactional(readOnly = true)
    public BugReportCommentFullDto findById(Integer id) {
        BugReportCommentEntity foundEntity = bugReportCommentRepository.findById(id)
                .orElseThrow(() -> new BugReportCommentNotFoundException(id));

        log.info("BugReportCommentServiceImpl -> found bugReportComment: {} by id: {} ", foundEntity, id);

        return BUG_REPORT_COMMENT_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BugReportCommentFullDto> findAll(int page, int size) {
        Page<BugReportCommentEntity> foundEntities = bugReportCommentRepository.findAll(PageRequest.of(page, size));

        Page<BugReportCommentFullDto> dto = foundEntities.map(entity -> BUG_REPORT_COMMENT_MAPPER.mapToFullDto(entity));

        log.info("BugReportCommentServiceImpl -> found {} bugReportComments", dto.getNumberOfElements());

        return dto;
    }

    @Override
    @Transactional
    public BugReportCommentFullDto create(BugReportCommentCreateDto createDto) {
        BugReportCommentEntity entityToSave = BUG_REPORT_COMMENT_MAPPER.mapToEntity(createDto);

        BugReportEntity bugReport = bugReportRepository.findById(createDto.getBugReportId())
                .orElseThrow(() -> new BugReportNotFoundException(createDto.getBugReportId()));

        entityToSave.setBugReport(bugReport);
        entityToSave.setCreateTime(Instant.now());

        BugReportCommentEntity savedEntity = bugReportCommentRepository.save(entityToSave);

        log.info("BugReportCommentServiceImpl -> bugReportComment {} successfully saved", savedEntity);

        return BUG_REPORT_COMMENT_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public BugReportCommentFullDto update(BugReportCommentUpdateDto updateDto) {
        BugReportCommentEntity entityToUpdate = bugReportCommentRepository.findById(updateDto.getId())
                .orElseThrow(() -> new BugReportCommentNotFoundException(updateDto.getId()));

        entityToUpdate.setMessage(updateDto.getMessage());
        entityToUpdate.setImageUrl(updateDto.getImageUrl());

        BugReportCommentEntity updatedEntity = bugReportCommentRepository.save(entityToUpdate);

        log.info("BugReportCommentServiceImpl -> bugReportComment {} successfully updated", updatedEntity);

        return BUG_REPORT_COMMENT_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        bugReportCommentRepository.findById(id)
                .orElseThrow(() -> new BugReportCommentNotFoundException(id));

        bugReportCommentRepository.deleteById(id);

        log.info("BugReportCommentServiceImpl -> bugReportComment by id: {} successfully deleted", id);
    }
}
