package by.itstep.feedback.service.impl;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentCreateDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentFullDto;
import by.itstep.feedback.dto.feedbackComment.FeedbackCommentUpdateDto;
import by.itstep.feedback.entity.FeedbackCommentEntity;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.exceptions.FeedbackCommentNotFoundException;
import by.itstep.feedback.exceptions.FeedbackNotFoundException;
import by.itstep.feedback.repository.FeedbackCommentRepository;
import by.itstep.feedback.repository.FeedbackRepository;
import by.itstep.feedback.service.FeedbackCommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.Instant;


import static by.itstep.feedback.mapper.FeedbackCommentMapper.FEEDBACK_COMMENT_MAPPER;

@Slf4j
@Service
public class FeedbackCommentServiceImpl implements FeedbackCommentService {

    @Autowired
    private FeedbackCommentRepository feedbackCommentRepository;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Override
    @Transactional(readOnly = true)
    public FeedbackCommentFullDto findById(Integer id) {
        FeedbackCommentEntity foundEntity = feedbackCommentRepository.findById(id)
                .orElseThrow(() -> new FeedbackCommentNotFoundException(id));

        log.info("FeedbackCommentServiceImpl -> found feedbackComment: {} by id: {} ", foundEntity, id);

        return FEEDBACK_COMMENT_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FeedbackCommentFullDto> findAll(int page, int size) {
        Page<FeedbackCommentEntity> foundEntities = feedbackCommentRepository.findAll(PageRequest.of(page, size));

        Page<FeedbackCommentFullDto> dto = foundEntities.map(entity -> FEEDBACK_COMMENT_MAPPER.mapToFullDto(entity));

        log.info("FeedbackCommentServiceImpl -> found {} feedbackComments", dto.getNumberOfElements());

        return dto;
    }

    @Override
    @Transactional
    public FeedbackCommentFullDto create(FeedbackCommentCreateDto createDto) {
        FeedbackCommentEntity entityToSave = FEEDBACK_COMMENT_MAPPER.mapToEntity(createDto);

        FeedbackEntity feedback = feedbackRepository.findById(createDto.getFeedbackId())
                .orElseThrow(() -> new FeedbackNotFoundException(createDto.getFeedbackId()));

        entityToSave.setFeedback(feedback);
        entityToSave.setCreateTime(Instant.now());

        FeedbackCommentEntity savedEntity = feedbackCommentRepository.save(entityToSave);

        log.info("FeedbackCommentServiceImpl -> feedbackComment {} successfully saved", savedEntity);
        
        return FEEDBACK_COMMENT_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public FeedbackCommentFullDto update(FeedbackCommentUpdateDto updateDto) {
        FeedbackCommentEntity entityToUpdate = feedbackCommentRepository.findById(updateDto.getId())
                .orElseThrow(() -> new FeedbackCommentNotFoundException(updateDto.getId()));

        entityToUpdate.setMessage(updateDto.getMessage());
        entityToUpdate.setImageUrl(updateDto.getImageUrl());

        FeedbackCommentEntity updatedEntity = feedbackCommentRepository.save(entityToUpdate);

        log.info("FeedbackCommentServiceImpl -> feedbackComment {} successfully updated", updatedEntity);

        return FEEDBACK_COMMENT_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
       feedbackCommentRepository.findById(id)
               .orElseThrow(() -> new FeedbackCommentNotFoundException(id));

        feedbackCommentRepository.deleteById(id);

        log.info("FeedbackCommentServiceImpl -> feedbackComment by id: {} successfully deleted", id);
    }
}
