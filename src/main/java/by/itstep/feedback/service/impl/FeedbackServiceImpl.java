package by.itstep.feedback.service.impl;

import by.itstep.feedback.dto.feedback.FeedbackCreateDto;
import by.itstep.feedback.dto.feedback.FeedbackFullDto;
import by.itstep.feedback.dto.feedback.FeedbackPreviewDto;
import by.itstep.feedback.dto.feedback.FeedbackUpdateDto;
import by.itstep.feedback.entity.FeedbackEntity;
import by.itstep.feedback.entity.enums.FeedbackEntityStatus;
import by.itstep.feedback.exceptions.FeedbackNotFoundException;
import by.itstep.feedback.repository.FeedbackRepository;
import by.itstep.feedback.service.FeedbackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.Instant;

import static by.itstep.feedback.mapper.FeedbackMapper.FEEDBACK_MAPPER;

@Slf4j
@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Override
    @Transactional(readOnly = true)
    public FeedbackFullDto findById(Integer id) {
        FeedbackEntity foundEntity = feedbackRepository.findById(id)
                .orElseThrow(() -> new FeedbackNotFoundException(id));

        log.info("FeedbackServiceImpl -> found feedBack: {} by id: {}", foundEntity, id);

        return FEEDBACK_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FeedbackPreviewDto> findAll(int page, int size) {
        Page<FeedbackEntity> foundEntities = feedbackRepository.findAll(PageRequest.of(page, size));

        Page<FeedbackPreviewDto> dto = foundEntities.map(entity -> FEEDBACK_MAPPER.mapToPreviewDto(entity));

        log.info("FeedbackServiceImpl -> found {} feedBacks", dto.getNumberOfElements());

        return dto;
    }

    @Override
    @Transactional
    public FeedbackFullDto create(FeedbackCreateDto createDto) {
        FeedbackEntity entityToSave = FEEDBACK_MAPPER.mapToEntity(createDto);

        entityToSave.setCreateTime(Instant.now());
        entityToSave.setStatus(FeedbackEntityStatus.NEW);

        FeedbackEntity savedEntity = feedbackRepository.save(entityToSave);

        log.info("FeedbackServiceImpl -> feedback {} successfully saved", savedEntity);

        return FEEDBACK_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public FeedbackFullDto update(FeedbackUpdateDto updateDto) {
        FeedbackEntity entityToUpdate = feedbackRepository.findById(updateDto.getId())
                .orElseThrow(() -> new FeedbackNotFoundException(updateDto.getId()));

        entityToUpdate.setMessage(updateDto.getMessage());

        FeedbackEntity updatedEntity = feedbackRepository.save(entityToUpdate);

        log.info("FeedbackServiceImpl -> feedback {} successfully updated", updatedEntity);

        return FEEDBACK_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        feedbackRepository.findById(id)
                .orElseThrow(() -> new FeedbackNotFoundException(id));

        feedbackRepository.deleteById(id);

        log.info("FeedbackServiceImpl -> feedback by id: {} successfully deleted", id);
    }
}
