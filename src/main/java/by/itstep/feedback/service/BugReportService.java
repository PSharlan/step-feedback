package by.itstep.feedback.service;

import by.itstep.feedback.dto.bugReport.BugReportCreateDto;
import by.itstep.feedback.dto.bugReport.BugReportFullDto;
import by.itstep.feedback.dto.bugReport.BugReportPreviewDto;
import by.itstep.feedback.dto.bugReport.BugReportUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BugReportService {

    BugReportFullDto findById(Integer id);

    Page<BugReportPreviewDto> findAll(int page, int size);

    BugReportFullDto create(BugReportCreateDto createDto);

    BugReportFullDto update(BugReportUpdateDto updateDto);

    void deleteById(Integer id);

}
