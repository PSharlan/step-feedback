package by.itstep.feedback.service;

import by.itstep.feedback.dto.bugReportComment.BugReportCommentCreateDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentFullDto;
import by.itstep.feedback.dto.bugReportComment.BugReportCommentUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BugReportCommentService {

    BugReportCommentFullDto findById(Integer id);

    Page<BugReportCommentFullDto> findAll(int page, int size);

    BugReportCommentFullDto create(BugReportCommentCreateDto createDto);

    BugReportCommentFullDto update(BugReportCommentUpdateDto updateDto);

    void deleteById(Integer id);

}
