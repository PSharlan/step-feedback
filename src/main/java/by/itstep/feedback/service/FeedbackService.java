package by.itstep.feedback.service;

import by.itstep.feedback.dto.feedback.FeedbackCreateDto;
import by.itstep.feedback.dto.feedback.FeedbackFullDto;
import by.itstep.feedback.dto.feedback.FeedbackPreviewDto;
import by.itstep.feedback.dto.feedback.FeedbackUpdateDto;
import org.springframework.data.domain.Page;

public interface FeedbackService {

    FeedbackFullDto findById(Integer id);

    Page<FeedbackPreviewDto> findAll(int page, int size);

    FeedbackFullDto create(FeedbackCreateDto createDto);

    FeedbackFullDto update(FeedbackUpdateDto updateDto);

    void deleteById(Integer id);

}
