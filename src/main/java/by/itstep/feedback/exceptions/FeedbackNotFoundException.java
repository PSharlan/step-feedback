package by.itstep.feedback.exceptions;

public class FeedbackNotFoundException extends EntityIsNotFoundException {
    public FeedbackNotFoundException(Integer id) {
        super("Feedback ", id);
    }
}
