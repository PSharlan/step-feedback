package by.itstep.feedback.exceptions;

public class BugReportCommentNotFoundException extends EntityIsNotFoundException {

    public BugReportCommentNotFoundException(Integer id) {
        super("BugReportComment ", id);
    }
}
