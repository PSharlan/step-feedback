package by.itstep.feedback.exceptions;

public class FeedbackCommentNotFoundException extends  EntityIsNotFoundException {

    public FeedbackCommentNotFoundException(Integer id) {
        super("FeedbackComment ", id);
    }

}
