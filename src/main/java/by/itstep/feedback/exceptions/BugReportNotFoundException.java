package by.itstep.feedback.exceptions;

public class BugReportNotFoundException extends EntityIsNotFoundException {

    public BugReportNotFoundException(Integer id) {
        super("BugReport ", id);
    }
}
